package com.example.androidpermissionmanager;



import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;















import java.util.jar.JarFile;



























import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AppDisplayLayout extends ActionBarActivity {
	Context context;
	NetworkInfo activeNetwork;
	Button UserApp,SystemApp,OpenPermission,GetButtons;
	String Buttonclick="UserApp";
	AlertDialog.Builder ListPopUp;
	int SelectedListItemNo;
	ListView BindAppToList;
	ProgressDialog dialog;
	Listadapter lstAdapter;
	LinearLayout LinearMenuBar;
	RelativeLayout RelativeLayoutMenu;
	ArrayList<String> AppName = new ArrayList<String>();
	ArrayList<String> ThirdPtyPackName = new ArrayList<String>();
	ArrayList<String> SystemAppPackName = new ArrayList<String>();
	ArrayList<Drawable> AppImage = new ArrayList<Drawable>();
	ArrayList<String> SysAppName = new ArrayList<String>();
	ArrayList<Drawable> SysAppImage = new ArrayList<Drawable>();
	HashMap<Integer,ArrayList<String>> Multimap=new HashMap<Integer,ArrayList<String>>();	
	ArrayList PermissionList=new ArrayList();
	String threatresult = "";
	HashMap<String, ArrayList<String>> threatAndroidPermissionMultimap2 = new HashMap<String, ArrayList<String>>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_app_display_layout);
		UserApp=(Button)findViewById(R.id.UserApp);
		SystemApp=(Button)findViewById(R.id.SystemApp);
		BindAppToList=(ListView)findViewById(R.id.BindAppToList);
		LinearMenuBar=(LinearLayout)findViewById(R.id.LinearMenuBar);
		RelativeLayoutMenu=(RelativeLayout)findViewById(R.id.RelativeLayoutMenu);
		context=this;
		GradianLayout(50,RelativeLayoutMenu,"#FFFFFF");
		GradianTextview(200,UserApp,"#FE2E64");
		GradianTextview(200,SystemApp,"#FFFFFF");

		NetworkInfo info = (NetworkInfo) ((ConnectivityManager)
			    context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

			    if (info == null)
			    {
			    	Toast.makeText(getApplicationContext(), "Check Internet connection", Toast.LENGTH_LONG).show();				        
			    }
			    else
			    {
			        if(info.isConnected())
			        {
			        	DummyAsyncTask task = new DummyAsyncTask();
			    		task.execute();			           
			        }
			    }
		//GetListValues();
//------------------------Listener UserApp--------------------------------------------------
		UserApp.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Handler handler=new Handler();
				handler.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						UserApp.getBackground().setAlpha(500);
						BindAppToList.setAdapter(null);	
						
						//Toast.makeText(getApplicationContext(), "text", Toast.LENGTH_SHORT).show();
						Buttonclick="UserApp";
						DummyAsyncTask task = new DummyAsyncTask();
						task.execute();
						
					}
				},300);
				UserApp.getBackground().setAlpha(100);
			}
		});
		
//------------------------Listener SystemApp--------------------------------------------------
		SystemApp.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Handler handler=new Handler();
				handler.postDelayed(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						SystemApp.getBackground().setAlpha(500);
						
						BindAppToList.setAdapter(null);
						Buttonclick="SystemApp";
						DummyAsyncTask task = new DummyAsyncTask();
						task.execute();
					}
					
				},200);
				SystemApp.getBackground().setAlpha(100);
			}
		});
	}

	
//----------------------Functions---------------------------------------------------	
	void GetListValues(){
		Handler handler=new Handler();
		handler.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				dialog.dismiss();	
				PackageManager pm = getPackageManager();
				List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
				for(ApplicationInfo packageInfo:packages){
				    if( pm.getLaunchIntentForPackage(packageInfo.packageName) != null ){
				                String currAppName = pm.getApplicationLabel(packageInfo).toString();
				                AppName.add(pm.getApplicationLabel(packageInfo).toString());
				                AppImage.add(getApplicationContext().getPackageManager().getApplicationIcon(packageInfo));
				                // Log.d("TAG", "Installed package :" + pm.getApplicationLabel(packageInfo).toString());
				                //Drawable icon;
				                //icon = getApplicationContext().getPackageManager().getApplicationIcon(packageInfo);
				                
				                //This app is a non-system app
				    }
				    else{
				        //System App
				    }
				    lstAdapter=new Listadapter(AppDisplayLayout.this,AppImage,AppName);
				    BindAppToList.setAdapter(lstAdapter);
				    
//-----------------------OnItemListListener--------------------------------------------	
				    
				    BindAppToList.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
								long arg3) {
						
							Toast.makeText(getApplicationContext(), "Hello",Toast.LENGTH_SHORT).show();
							
						}
					});
				    
				    
			  }
			}
		},2000);
		dialog = new ProgressDialog(context);
		dialog.setCancelable(false);
		dialog.setTitle("Loading....");
		dialog.setMessage("Searching");
		dialog.show();
	}
	
//-------------------------ListAdapter-----------------------------------------------
	class Listadapter extends BaseAdapter{
		Context context;
		ArrayList<Drawable> AppImage=new ArrayList<Drawable>();
		ArrayList<String> AppName=new ArrayList<String>();
		
			public Listadapter(Context c,ArrayList<Drawable> AppImage, ArrayList<String> AppName){
				this.context=c;
				this.AppImage=AppImage;
				this.AppName=AppName;
			}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return AppImage.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflator=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			View row=inflator.inflate(R.layout.listview_displaylayout,parent,false);
			ImageView AppImage1=(ImageView)row.findViewById(R.id.AppImage);
			TextView AppName1=(TextView)row.findViewById(R.id.AppName);
			AppImage1.setImageDrawable(AppImage.get(position));
			AppName1.setText(AppName.get(position));
			return row;
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.app_display_layout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
//--------------------------AsyncTask------------------------------------------------
	
	public class DummyAsyncTask extends AsyncTask<String, Void, Void>
	{
		ProgressDialog dialog;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(context);
			dialog.setCancelable(false);
			/*Handler handler=new Handler();
			handler.postDelayed(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					
				}
				
			},2000);*/
			dialog.setTitle("Loading....");
			dialog.setMessage("Searching");
			dialog.show();
			
		}
		protected Void doInBackground(String... arg0) {
				PackageManager pm = getPackageManager();
			
			
			 
			 List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
			int multilistcount=0;
			 for(ApplicationInfo packageInfo:packages){
			    if( pm.getLaunchIntentForPackage(packageInfo.packageName) != null ){
			    	ThirdPtyPackName.add(packageInfo.packageName);
			                String currAppName = pm.getApplicationLabel(packageInfo).toString();
			                AppName.add(pm.getApplicationLabel(packageInfo).toString());
			                AppImage.add(getApplicationContext().getPackageManager().getApplicationIcon(packageInfo));
			                String Path=packageInfo.packageName.toString();
			                try {
								PackageInfo packageInfo2 = pm.getPackageInfo(packageInfo.packageName, PackageManager.GET_PERMISSIONS);
								String[] requestedPermissions = packageInfo2.requestedPermissions;

							      if(requestedPermissions != null) {
							         for (int i = 0; i < requestedPermissions.length; i++) {
							           //Log.d(Path, requestedPermissions[i]);
							            PermissionList.add(requestedPermissions[i]);					          
							         }
							         Multimap.put(multilistcount, new ArrayList<String>(PermissionList));
							       //  Log.d("test", "--------------------------------");
							         
//------------------------------------------------------------------------------------------------------------------------------							        
							         try {
							             String installer = context.getPackageManager()
							                                         .getInstallerPackageName(context.getPackageName());
							             Log.d("tag", installer);
							         } catch (Throwable e) {          
							         }
							         String appPackageName=packageInfo.packageName;
							         Uri a=Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName);
							      String aa=a.toString();
							        
							      }
							      else{
							    	  PermissionList.add("No Permission Access");
							    	  Multimap.put(multilistcount, new ArrayList<String>(PermissionList));
									    
							      }
			                } catch (NameNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} 
			                PermissionList.clear();
			                multilistcount++;
			    }
			    else{
			        //System App
			    	String currAppName = pm.getApplicationLabel(packageInfo).toString();
			    	SystemAppPackName.add(packageInfo.packageName);
	                SysAppName.add(pm.getApplicationLabel(packageInfo).toString());
	                SysAppImage.add(getApplicationContext().getPackageManager().getApplicationIcon(packageInfo));        
			    }
			    
			
		   }

			String threaturl = "http://androidpermission.comuv.com/ThreatsTable.php";
			InputStream threatinputStream = null;

			try {
				HttpClient threathttpclient = new DefaultHttpClient();

				HttpResponse threathttpResponse = threathttpclient.execute(new HttpGet(threaturl));
				threatinputStream = threathttpResponse.getEntity().getContent();

				if (threatinputStream != null) {
					BufferedReader threatbufferedReader = new BufferedReader(
							new InputStreamReader(threatinputStream, "UTF-8"));
					String threatline = "";
					while ((threatline = threatbufferedReader.readLine()) != null)
						threatresult += threatline;
					System.out.println(threatresult);
					threatinputStream.close();
				} else
					threatresult = "Did not work!";

			} catch (Exception e) {
				System.out.println("InputStream" + e.getLocalizedMessage());
			}
			//from here it starts

			try {
				JSONArray threatJarray = new JSONArray(threatresult);
				for (int i = 0; i < threatJarray.length(); i++) {
					JSONObject threatJasonobject = null;
					threatJasonobject = threatJarray.getJSONObject(i);
					{
						threatAndroidPermissionMultimap2.put(
								threatJasonobject.getString("Android Application Permissions,Threats"),
								new ArrayList<String>(Arrays.asList(threatJasonobject.getString("Android.Viser. A"),
										threatJasonobject.getString("Android Mobclick. A"),
										threatJasonobject.getString("Android.Agent.CE"),
										threatJasonobject.getString("Android.Domob.A"),
										threatJasonobject.getString("Android.Airpush.D"),
										threatJasonobject.getString("Android.Agent.DL"),
										threatJasonobject.getString("Android.AdMongo.A"),
										threatJasonobject.getString("Android.Infostealer.C"),
										threatJasonobject.getString("Android. Leadbolt.A"),
										threatJasonobject.getString("Android.FakeRun.A"),
										threatJasonobject.getString("Android.Wabek.A"),
										threatJasonobject.getString("Android.Oldboot"),
										threatJasonobject.getString("Android.MobileSpyware.SmsTracker"),
										threatJasonobject.getString("Android.Trojan.Sms.Send.B"),
										threatJasonobject.getString("Android.Bot.Notcompatible"),

										threatJasonobject.getString("Android.MobileSpyware.Ackposts"),
										threatJasonobject.getString("Android.Trojan.Wapsx"),
										threatJasonobject.getString("Android.MobileSpyware.SpyBubbl"),
										threatJasonobject.getString("Android.Backdoor.Advulna"),
										threatJasonobject.getString("Android.MobileSpyware.SpyMob.a"),
										threatJasonobject.getString("Android.Adware.Kuguo.A"),
										threatJasonobject.getString("Android.Backdoor.Ikangoo"),
										threatJasonobject.getString("Android.MobileSpyware.Spyoo"),
										threatJasonobject.getString("Android.Trojan.GGTracker"),
										threatJasonobject.getString("Android.Adware.ImadPush.A"),
										threatJasonobject.getString("Android.Downloader.Morepaks"),
										threatJasonobject.getString("Android.Airpush.G"),

										threatJasonobject.getString("Android.Gedma.A"),
										threatJasonobject.getString("Android.SMSreg.V"),
										threatJasonobject.getString("Android.Gemni.A"),
										threatJasonobject.getString("Android.SecApk.A"),
										threatJasonobject.getString("Android.VirusShield.A"),
										threatJasonobject.getString("Android.Agent.HD"),
										threatJasonobject.getString("Android.Trojan.Coogos.A!tr"),
										threatJasonobject.getString("Android.Adware.Uapush.A"),
										threatJasonobject.getString("Android.Dowgin.T"),
										threatJasonobject.getString("Android.Coogos.Ad388"),
										threatJasonobject.getString("Android.SmsThief.AI"),
										threatJasonobject.getString("Android.Fobus.A"),

										threatJasonobject.getString("Android.Airpush.C"),
										threatJasonobject.getString("Android.Smsreg.W"),
										threatJasonobject.getString("Android.Opfake.AG"),
										threatJasonobject.getString("Android.Simplocker.D"),
										threatJasonobject.getString("Android.Inis.A"),
										threatJasonobject.getString("Android.SeaWeth.D"),
										threatJasonobject.getString("Android.Dingwe.A"),
										threatJasonobject.getString("Android.Koler.A"),
										threatJasonobject.getString("Android.Simplocker.A"),
										threatJasonobject.getString("Android.Selfmite.B"),
										threatJasonobject.getString("Android.MobileSpyware.Kasandra"),
										threatJasonobject.getString("Android.MobileSpyware.Gappusin"),

										threatJasonobject.getString("Android.Trojan.FakeFlash"),
										threatJasonobject.getString("Android.MobileSpyware.CellSpy"),
										threatJasonobject.getString("Android.MobileSpyware.Tekwon.A"),
										threatJasonobject.getString("Android.Trojan.Qdplugin"),
										threatJasonobject.getString("Android.ScareWare.Slocker.A"),
										threatJasonobject.getString("Android.Agent.KB"),
										threatJasonobject.getString("Android.Adend.A"),
										threatJasonobject.getString("Android.Wroba.A"),
										threatJasonobject.getString("Android.RevMobAD.A"),
										threatJasonobject.getString("Android.SmsTheif.BG"),
										threatJasonobject.getString("Android.Inmobi.A"),
										threatJasonobject.getString("Android.Svpeng.K"),

										threatJasonobject.getString("Trojan-SMS.AndroidOS.Podec.a"),
										threatJasonobject.getString("DangerousObject.Multi.Generic"),
										threatJasonobject.getString("Trojan.AndroidOS.Rootnik.a"),
										threatJasonobject.getString("Trojan-SMS.AndroidOS.Opfake.a"),
										threatJasonobject.getString("Backdoor.AndroidOS.Obad.f"),
										threatJasonobject.getString("Trojan-Downloader.AndroidOS.Leech.a"),
										threatJasonobject.getString("Exploit.AndroidOS.Lotoor.be"),
										threatJasonobject.getString("Trojan.AndroidOS.Ztorg.a"),
										threatJasonobject.getString("Trojan-Dropper.AndroidOS.Gorpo.a"),
										threatJasonobject.getString("Trojan.AndroidOS.Fadeb.a"),
										threatJasonobject.getString("Trojan-SMS.AndroidOS.Stealer.a"),
										threatJasonobject.getString("Exploit.AndroidOS.Lotoor.a"),

										threatJasonobject.getString("Trojan-SMS.AndroidOS.Opfake.bo"),
										threatJasonobject.getString("Trojan.AndroidOS.Ztorg.b"),
										threatJasonobject.getString("Trojan.AndroidOS.Mobtes.b"),
										threatJasonobject.getString("Trojan-SMS.AndroidOS.FakeInst.fz"),
										threatJasonobject.getString("Trojan.AndroidOS.Ztorg.pac"),
										threatJasonobject.getString("Trojan-SMS.AndroidOS.FakeInst.hb"),
										threatJasonobject.getString("Android.Smsreg.DA"),
										threatJasonobject.getString("Android.Sprovider.A"),
										threatJasonobject.getString("Android.Ztorg.A"),
										threatJasonobject.getString("Android.Leech.E"),
										threatJasonobject.getString("Android.Rootnik.C"),
										threatJasonobject.getString("Android.Reaper.A"),

										threatJasonobject.getString("Android.CallPay.A"),
										threatJasonobject.getString("Android.Senrec.A"),
										threatJasonobject.getString("Android.Mkero.A"),
										threatJasonobject.getString("Android.Feabme.A")

								)));
					}
				}

				System.out.println("In app purchase New/n" + threatAndroidPermissionMultimap2.get("In app purchase"));

					/*ArrayList<String> apppermissions = new ArrayList<String>();
					apppermissions.add("Device & app history");
					apppermissions.add("Identity");*/

				//System.out.println("here");
				//System.out.println("Sum=" + sum);

			} catch (Exception e1) {

				System.out.println("Error parsing data " + e1.toString());
			}

			return null;
		}
		 @Override
			protected void onPostExecute(Void result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				
				dialog.dismiss();
				if(Buttonclick.equalsIgnoreCase("UserApp")){
				lstAdapter=new Listadapter(AppDisplayLayout.this,AppImage,AppName);
				BindAppToList.setAdapter(lstAdapter);
//-----------------------OnItemListListener--------------------------------------------	
			    
			    BindAppToList.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
							long arg3) {
						SelectedListItemNo=arg2;
						LayoutInflater inflater = getLayoutInflater();
						View ListPopUpWindow = inflater.inflate(R.layout.list_popup_window, null);
						ListPopUp=new AlertDialog.Builder(AppDisplayLayout.this);
						final AlertDialog alert = ListPopUp.create();
						alert.setView(ListPopUpWindow);
						alert.setCancelable(true);   
						//alert.show().getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
						alert.show();
						OpenPermission=(Button)ListPopUpWindow.findViewById(R.id.OpenPermissions);			    
					    OpenPermission.setOnClickListener(new View.OnClickListener() {
							
							@Override
							public void onClick(View v) {
								
									alert.dismiss();
									Handlercall(OpenPermission);
									Intent intent=new Intent(AppDisplayLayout.this,ViewAppPermissions.class);
									Bundle extras = new Bundle();
									extras.putSerializable("HashMap",Multimap);
									intent.putExtras(extras);
								Bundle extra1=new Bundle();
								extra1.putSerializable("HashMap1",threatAndroidPermissionMultimap2);
								intent.putExtras(extra1);
									intent.putExtra("ThirdPtyPackName",ThirdPtyPackName);
									intent.putExtra("SystemAppPackName",SystemAppPackName);
									intent.putExtra("SelectedListItemNo", SelectedListItemNo);
									startActivity(intent);
									alert.dismiss();
									alert.cancel();
								/*}else{
									Toast.makeText(getApplicationContext(), "Check Internet connection", Toast.LENGTH_SHORT).show();
									
								}*/
							}
						});
					}
				});
				}
				//lstAdapter.notifyDataSetChanged();
				
				else{
				lstAdapter=new Listadapter(AppDisplayLayout.this,SysAppImage,SysAppName);
			    BindAppToList.setAdapter(lstAdapter);
			    //lstAdapter.notifyDataSetChanged();
				}
			    BindAppToList.setBackgroundColor(Color.BLACK);
			
		 }
		
	}
	void GradianLayout(int num,RelativeLayout name,String color)
	{
		GradientDrawable gd = new GradientDrawable();
	    gd.setColor(Color.parseColor(color));
	    gd.setCornerRadius(num);
	   // name.setBackgroundDrawable(gd);
	}
	void GradianTextview(int num,Button name,String color)
	{
		GradientDrawable gd = new GradientDrawable();
	    gd.setColor(Color.parseColor(color));
	    gd.setCornerRadius(num);
	   // name.setBackgroundDrawable(gd);
	}
	void Handlercall(Button GetButton){
		GetButtons=GetButton;
		Handler handler=new Handler();
		handler.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				GetButtons.getBackground().setAlpha(500);
				
			}
		}, 200);
		GetButtons.getBackground().setAlpha(200);
	}
	
}
