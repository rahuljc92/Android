package com.example.androidpermissionmanager;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.DocumentsContract.Document;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ViewAppPermissions extends ActionBarActivity {
    ListView List_Permissions;
    TextView TxtCatagoryName, TxtPaidOrFree, TxtSubcatName, TxtSubcatName2;
    int SelectedListItemNo;
    int ElementExists = 0, CatagoryExist = 0;
    String PackageName, PaidorFree, AppCatagoryName, SubcatName, CpyAppPurchase, CpyAddSupport;
    String connectionURL = "Sucess";
    org.jsoup.nodes.Document html = null;
    org.jsoup.nodes.Document htmlExtract = null;
    NetworkInfo activeNetwork;
    ArrayList<String> ThirdPtyPackName = new ArrayList<String>();
    ArrayList<String> keys = new ArrayList<String>();
    ArrayList<String> AddCheckCatagoryList = new ArrayList<String>();
    ArrayList<String> myKey = new ArrayList<String>();
    ArrayList<String> addallArrayList = new ArrayList<String>();
    ArrayList PermissionList = new ArrayList();
    ArrayList PermissionListCopy = new ArrayList();
    ArrayList NopermissionMatchList = new ArrayList();
    ArrayList<String> GetListFromHashMap = new ArrayList<String>();
    HashMap<Integer, ArrayList<String>> Multimap = new HashMap<Integer, ArrayList<String>>();
    HashMap<String, ArrayList<String>> GetAndPerMap = new HashMap<String, ArrayList<String>>();
    LinkedHashMap<String, ArrayList<String>> GetAndPerMap1 = new LinkedHashMap<String, ArrayList<String>>();
    HashMap<String, ArrayList<String>> AndroidPermissionMultimap = new HashMap<String, ArrayList<String>>();
    HashMap<String, HashMap<String, ArrayList<String>>> HashInHash = new HashMap<String, HashMap<String, ArrayList<String>>>();
    LinkedHashMap<String, ArrayList<String>> sortedMap = new LinkedHashMap<String, ArrayList<String>>();
    String raresult = "";
    HashMap<String, ArrayList<String>> raAndroidPermissionMultimap1 = new HashMap<String, ArrayList<String>>();
    HashMap<String, ArrayList<String>> threatAndroidPermissionMultimap1 = new HashMap<String, ArrayList<String>>();
    HashMap<String, ArrayList<String>> threatAndroidPermissionMultimap2 = new HashMap<String, ArrayList<String>>();
    HashMap<String, ArrayList<String>> threatAndroidPermissionMultimap3 = new HashMap<String, ArrayList<String>>();
    HashMap<String, ArrayList<String>> adsMap = new HashMap<String, ArrayList<String>>();
    String threatresult = "";
    String threatresult1 = "";
    String threatresult5 = "";
    String toalnoofthreats;
    String toalnoofthreats1;
    int totalthreats = 0;
    ArrayList<String> listOfPermissions = new ArrayList<String>();
    int totalThreats;
    String feature5 = "0";
    String feature4 = "0";
    String feature3 = "0";
    String feature2 = "0";
    String feature1 = "0";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_app_permission);
        List_Permissions = (ListView) findViewById(R.id.List_Permissions);
        List_Permissions.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        List_Permissions.setItemsCanFocus(false);
        TxtCatagoryName = (TextView) findViewById(R.id.CatagoryName);
        TxtSubcatName2 = (TextView) findViewById(R.id.TxtSubcatName2);
        TxtSubcatName = (TextView) findViewById(R.id.TxtSubcatName);
        TxtPaidOrFree = (TextView) findViewById(R.id.PaidOrFree);
        Bundle bundle = this.getIntent().getExtras();
        Multimap = (HashMap<Integer, ArrayList<String>>) bundle.getSerializable("HashMap");
        SelectedListItemNo = bundle.getInt("SelectedListItemNo");
        ThirdPtyPackName = bundle.getStringArrayList("ThirdPtyPackName");
        threatAndroidPermissionMultimap2 = (HashMap<String, ArrayList<String>>) bundle.getSerializable("HashMap1");
        PackageName = ThirdPtyPackName.get(SelectedListItemNo);
        AndroidPermissionCollection GetListData = new AndroidPermissionCollection();

        AndroidPermissionMultimap = GetListData.AndroidpermissionsCollectionList();


//------------------------------Listing permission in order for each application---------------------------		
        for (int count = 0; count < Multimap.get(SelectedListItemNo).size(); count++) {
            PermissionListCopy.add(Multimap.get(SelectedListItemNo).get(count));
            //Log.d("Track",Multimap.get(SelectedListItemNo).get(count));
        }
//-----------------------------Selecting permission from Android permission class----------------------------
        for (int count = 0; count < PermissionListCopy.size(); count++) {
            if (AndroidPermissionMultimap.get(PermissionListCopy.get(count)) != null) {
                GetAndPerMap.put((String) PermissionListCopy.get(count), AndroidPermissionMultimap.get(PermissionListCopy.get(count)));
            } else {
                NopermissionMatchList.add(PermissionListCopy.get(count));
            }
        }
//---------------------------------------------------------------------------------------------------------------		
        PermissionList.addAll(GetAndPerMap.keySet());
//--------------------------------------Sorting permissions------------------------------------------------------
        if (GetAndPerMap.size() != 0) {
            for (int count = 0; count < PermissionList.size(); count++) {
                if (count == 0) {
                    sortedMap.put((String) PermissionList.get(count), GetAndPerMap.get(PermissionList.get(count)));
                    addallArrayList.addAll(GetAndPerMap.get(PermissionList.get(count)));
                    AddCheckCatagoryList.add(addallArrayList.get(0));
                    Log.d("ff", addallArrayList.get(0));
                    Log.d("ff", addallArrayList.get(1));
                    Log.d("ff", addallArrayList.get(2));

                    for (int pr = 0; pr < addallArrayList.size(); pr++) {
                        if (addallArrayList.get(pr).equalsIgnoreCase("0") || (addallArrayList.get(pr).equalsIgnoreCase("1"))) {
                            System.out.println("");
                        } else {
                            listOfPermissions.add(addallArrayList.get(pr));
                        }
                    }
                    addallArrayList.clear();
                } else {
                    for (int check = 0; check < AddCheckCatagoryList.size(); check++) {
                        if (GetAndPerMap.get(PermissionList.get(count)).get(0).equals(AddCheckCatagoryList.get(check))) {
                            CatagoryExist = 1;

                        }
                    }
                    if (CatagoryExist != 1) {
                        addallArrayList.clear();
                        sortedMap.put((String) PermissionList.get(count), GetAndPerMap.get(PermissionList.get(count)));
                        AddCheckCatagoryList.add(GetAndPerMap.get(PermissionList.get(count)).get(0));

                    }
                    if (CatagoryExist != 1) {
                        //sortedMap.put((String) PermissionList.get(count), GetAndPerMap.get(PermissionList.get(count)));
                        addallArrayList.addAll(GetAndPerMap.get(PermissionList.get(count)));
                        //AddCheckCatagoryList.add(addallArrayList.get(0));
                        Log.d("ff", addallArrayList.get(0));
                        Log.d("ff", addallArrayList.get(1));
                        Log.d("ff", addallArrayList.get(2));
                        for (int pr = 0; pr < addallArrayList.size(); pr++) {
                            if (addallArrayList.get(pr).equalsIgnoreCase("0") || (addallArrayList.get(pr).equalsIgnoreCase("1"))) {

                            } else
                                listOfPermissions.add(addallArrayList.get(pr));
                        }
                        addallArrayList.clear();
                    }

                }
                if (CatagoryExist != 1) {
                    for (int count2 = count + 1; count2 < PermissionList.size(); count2++) {
                        if (GetAndPerMap.get(PermissionList.get(count)).get(0).equals(GetAndPerMap.get(PermissionList.get(count2)).get(0))) {
                            sortedMap.put((String) PermissionList.get(count2), GetAndPerMap.get(PermissionList.get(count2)));
                            addallArrayList.addAll(GetAndPerMap.get(PermissionList.get(count2)));

                        }
                    }
                }
                CatagoryExist = 0;
                Log.d("end", "----------------------");
            }
            myKey.addAll(sortedMap.keySet());
        } else {
            sortedMap.put("0", NopermissionMatchList);
        }


        Listadapter lstadapter1 = new Listadapter(ViewAppPermissions.this, sortedMap, myKey);
        List_Permissions.setAdapter(lstadapter1);
        DummyAsyncTask task = new DummyAsyncTask();
        task.execute();

    }

    //New asynchronous task


	/*public class secondaryAsyncTask extends AsyncTask<String, Void, Void>
    {
		@Override
		protected Void doInBackground(String... params) {
			String threaturl1 = "http://androidpermission.comuv.com/ThreatsTable.php";
			InputStream threatinputStream1 = null;
			System.out.println("BBBBBBBBBBBBBBB");

			try {
				HttpClient threathttpclient1 = new DefaultHttpClient();

				HttpResponse threathttpResponse1 = threathttpclient1.execute(new HttpGet(threaturl1));
				threatinputStream1 = threathttpResponse1.getEntity().getContent();

				if (threatinputStream1 != null) {
					BufferedReader threatbufferedReader1 = new BufferedReader(
							new InputStreamReader(threatinputStream1, "UTF-8"));
					String threatline1 = "";
					while ((threatline1 = threatbufferedReader1.readLine()) != null)
						threatresult1 += threatline1;
					System.out.println(threatresult1);
					threatinputStream1.close();
				} else
					threatresult1 = "Did not work!";

			} catch (Exception e) {
				System.out.println("InputStream" + e.getLocalizedMessage());
			}
			return null;
		}
		protected void onPostExecute(Void v) {
			try {
				JSONArray threatJarray1 = new JSONArray(threatresult);
				for (int i = 0; i < threatJarray1.length(); i++) {
					JSONObject threatJasonobject1 = null;
					threatJasonobject1 = threatJarray1.getJSONObject(i);
					{
						threatAndroidPermissionMultimap2.put(
								threatJasonobject1.getString("Android Application Permissions,Threats"),
								new ArrayList<String>(Arrays.asList(threatJasonobject1.getString("Android.Viser. A"),
										threatJasonobject1.getString("Android Mobclick. A"),
										threatJasonobject1.getString("Android.Agent.CE"),
										threatJasonobject1.getString("Android.Domob.A"),
										threatJasonobject1.getString("Android.Airpush.D"),
										threatJasonobject1.getString("Android.Agent.DL"),
										threatJasonobject1.getString("Android.AdMongo.A"),
										threatJasonobject1.getString("Android.Infostealer.C"),
										threatJasonobject1.getString("Android. Leadbolt.A"),
										threatJasonobject1.getString("Android.FakeRun.A"),
										threatJasonobject1.getString("Android.Wabek.A"),
										threatJasonobject1.getString("Android.Oldboot"),
										threatJasonobject1.getString("Android.MobileSpyware.SmsTracker"),
										threatJasonobject1.getString("Android.Trojan.Sms.Send.B"),
										threatJasonobject1.getString("Android.Bot.Notcompatible"),

										threatJasonobject1.getString("Android.MobileSpyware.Ackposts"),
										threatJasonobject1.getString("Android.Trojan.Wapsx"),
										threatJasonobject1.getString("Android.MobileSpyware.SpyBubbl"),
										threatJasonobject1.getString("Android.Backdoor.Advulna"),
										threatJasonobject1.getString("Android.MobileSpyware.SpyMob.a"),
										threatJasonobject1.getString("Android.Adware.Kuguo.A"),
										threatJasonobject1.getString("Android.Backdoor.Ikangoo"),
										threatJasonobject1.getString("Android.MobileSpyware.Spyoo"),
										threatJasonobject1.getString("Android.Trojan.GGTracker"),
										threatJasonobject1.getString("Android.Adware.ImadPush.A"),
										threatJasonobject1.getString("Android.Downloader.Morepaks"),
										threatJasonobject1.getString("Android.Airpush.G"),

										threatJasonobject1.getString("Android.Gedma.A"),
										threatJasonobject1.getString("Android.SMSreg.V"),
										threatJasonobject1.getString("Android.Gemni.A"),
										threatJasonobject1.getString("Android.SecApk.A"),
										threatJasonobject1.getString("Android.VirusShield.A"),
										threatJasonobject1.getString("Android.Agent.HD"),
										threatJasonobject1.getString("Android.Trojan.Coogos.A!tr"),
										threatJasonobject1.getString("Android.Adware.Uapush.A"),
										threatJasonobject1.getString("Android.Dowgin.T"),
										threatJasonobject1.getString("Android.Coogos.Ad388"),
										threatJasonobject1.getString("Android.SmsThief.AI"),
										threatJasonobject1.getString("Android.Fobus.A"),

										threatJasonobject1.getString("Android.Airpush.C"),
										threatJasonobject1.getString("Android.Smsreg.W"),
										threatJasonobject1.getString("Android.Opfake.AG"),
										threatJasonobject1.getString("Android.Simplocker.D"),
										threatJasonobject1.getString("Android.Inis.A"),
										threatJasonobject1.getString("Android.SeaWeth.D"),
										threatJasonobject1.getString("Android.Dingwe.A"),
										threatJasonobject1.getString("Android.Koler.A"),
										threatJasonobject1.getString("Android.Simplocker.A"),
										threatJasonobject1.getString("Android.Selfmite.B"),
										threatJasonobject1.getString("Android.MobileSpyware.Kasandra"),
										threatJasonobject1.getString("Android.MobileSpyware.Gappusin"),

										threatJasonobject1.getString("Android.Trojan.FakeFlash"),
										threatJasonobject1.getString("Android.MobileSpyware.CellSpy"),
										threatJasonobject1.getString("Android.MobileSpyware.Tekwon.A"),
										threatJasonobject1.getString("Android.Trojan.Qdplugin"),
										threatJasonobject1.getString("Android.ScareWare.Slocker.A"),
										threatJasonobject1.getString("Android.Agent.KB"),
										threatJasonobject1.getString("Android.Adend.A"),
										threatJasonobject1.getString("Android.Wroba.A"),
										threatJasonobject1.getString("Android.RevMobAD.A"),
										threatJasonobject1.getString("Android.SmsTheif.BG"),
										threatJasonobject1.getString("Android.Inmobi.A"),
										threatJasonobject1.getString("Android.Svpeng.K"),

										threatJasonobject1.getString("Trojan-SMS.AndroidOS.Podec.a"),
										threatJasonobject1.getString("DangerousObject.Multi.Generic"),
										threatJasonobject1.getString("Trojan.AndroidOS.Rootnik.a"),
										threatJasonobject1.getString("Trojan-SMS.AndroidOS.Opfake.a"),
										threatJasonobject1.getString("Backdoor.AndroidOS.Obad.f"),
										threatJasonobject1.getString("Trojan-Downloader.AndroidOS.Leech.a"),
										threatJasonobject1.getString("Exploit.AndroidOS.Lotoor.be"),
										threatJasonobject1.getString("Trojan.AndroidOS.Ztorg.a"),
										threatJasonobject1.getString("Trojan-Dropper.AndroidOS.Gorpo.a"),
										threatJasonobject1.getString("Trojan.AndroidOS.Fadeb.a"),
										threatJasonobject1.getString("Trojan-SMS.AndroidOS.Stealer.a"),
										threatJasonobject1.getString("Exploit.AndroidOS.Lotoor.a"),

										threatJasonobject1.getString("Trojan-SMS.AndroidOS.Opfake.bo"),
										threatJasonobject1.getString("Trojan.AndroidOS.Ztorg.b"),
										threatJasonobject1.getString("Trojan.AndroidOS.Mobtes.b"),
										threatJasonobject1.getString("Trojan-SMS.AndroidOS.FakeInst.fz"),
										threatJasonobject1.getString("Trojan.AndroidOS.Ztorg.pac"),
										threatJasonobject1.getString("Trojan-SMS.AndroidOS.FakeInst.hb"),
										threatJasonobject1.getString("Android.Smsreg.DA"),
										threatJasonobject1.getString("Android.Sprovider.A"),
										threatJasonobject1.getString("Android.Ztorg.A"),
										threatJasonobject1.getString("Android.Leech.E"),
										threatJasonobject1.getString("Android.Rootnik.C"),
										threatJasonobject1.getString("Android.Reaper.A"),

										threatJasonobject1.getString("Android.CallPay.A"),
										threatJasonobject1.getString("Android.Senrec.A"),
										threatJasonobject1.getString("Android.Mkero.A"),
										threatJasonobject1.getString("Android.Feabme.A")

								)));
					}
				}

				System.out.println("AAAAAAAAAAAAAAAAA/n" + threatAndroidPermissionMultimap2.get("In app purchase"));
				//int[] threatarray1 = new int[100];
				//for (int i = 0; i < 100; i++) {
				//	threatarray1[i] = 0;
				//}
					/*ArrayList<String> apppermissions = new ArrayList<String>();
					apppermissions.add("Device & app history");
					apppermissions.add("Identity");*/


				/*for (int k = 0; k < listOfPermissions.size(); k++) {
                    //System.out.println("PermissionList.size()" + listOfPermissions.size());
					ArrayList<String> arraylist1 = new ArrayList<String>();
					try {
						arraylist1.addAll(threatAndroidPermissionMultimap2.get(listOfPermissions.get(k)));
					}
					catch(NullPointerException e)
					{
						continue;
					}
					System.out.println("Check"+listOfPermissions.get(k) + "\n");
					for (int i = 0; i < arraylist1.size(); i++) {
						if (arraylist1.get(i).equalsIgnoreCase("1")) {
							threatarray1[i] = 1;
							//	System.out.println("Permission " + arraylist.get(i) + "\n");
						}
					}
				}
				//System.out.println("here");
				int j = 0;
				int sum = 0;
				for (int i = 0; i < 99; i++) {
					sum = sum + threatarray1[i];
				}
				//System.out.println("Sum=" + sum);
				toalnoofthreats1=Integer.toString(sum);


			} catch (Exception e1) {

				System.out.println("Error parsing data " + e1.toString());
			}
		}

	}*/


    public class DummyAsyncTask extends AsyncTask<String, Void, Void> {
        private ProgressDialog progressDialog = new ProgressDialog(ViewAppPermissions.this);
        InputStream is = null;
        String result = "";

        protected void onPreExecute() {
            progressDialog.setMessage("Fetching data...");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                System.out.println("We are here1 \n");
                Context context;
                ConnectivityManager cm = (ConnectivityManager) getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);
                activeNetwork = cm.getActiveNetworkInfo();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Check Internet connection", Toast.LENGTH_SHORT).show();
            }
            //String url="http://androidpermission.comuv.com/PermissionList.php";
            String url = "https://play.google.com/store/apps/details?id=" + PackageName + "&hl=en";
            String url_selec1 = "http://hmkcode.appspot.com/rest/controller/get.json";
            InputStream inputStream = null;
            String result = "";
            try {

                // create HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
                inputStream = httpResponse.getEntity().getContent();
                try {
                    html = Jsoup.connect(url).get();
                } catch (final Exception e) {
                    Log.d("error", e.toString());
                    TxtCatagoryName.setText("App Not found in Google Play Store");
                    connectionURL = "Failed";
                    return null;
                }
                if (inputStream != null) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                    //	BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null)
                        result += line;
                    inputStream.close();
                } else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
                TxtCatagoryName.setText("App Not found in Google Play Store");
                //TxtSubcatName2.setText("App Not found in Google Play Store");
                connectionURL = "Failed";
            }
            // TODO Auto-generated method stub

            String raurl = "http://androidpermission.comuv.com/CategoryStatics.php";
            //String raurl_select = "http://tekart.byethost9.com/demo.php";
            //String raurl_selec1 = "http://hmkcode.appspot.com/rest/controller/get.json";
            InputStream rainputStream = null;

            try {
                HttpClient rahttpclient = new DefaultHttpClient();

                HttpResponse rahttpResponse = rahttpclient.execute(new HttpGet(raurl));
                rainputStream = rahttpResponse.getEntity().getContent();

                if (rainputStream != null) {
                    BufferedReader rabufferedReader = new BufferedReader(new InputStreamReader(rainputStream, "UTF-8"));
                    String line = "";
                    while ((line = rabufferedReader.readLine()) != null)
                        raresult += line;
                    System.out.println(raresult);
                    rainputStream.close();
                } else
                    raresult = "Did not work!";

            } catch (Exception e) {
                Log.d("Rahul ", e.toString());
                System.out.println("InputStream" + e.getLocalizedMessage());
            }


            String threaturlAds = "http://androidpermission.comuv.com/Adstable2.php";
            InputStream threatinputStreamAds = null;
            String threatresultAds = "";
            try {
                HttpClient threathttpclientAds = new DefaultHttpClient();

                HttpResponse threathttpResponseAds = threathttpclientAds.execute(new HttpGet(threaturlAds));
                threatinputStreamAds = threathttpResponseAds.getEntity().getContent();

                if (threatinputStreamAds != null) {
                    BufferedReader threatbufferedReaderAds = new BufferedReader(
                            new InputStreamReader(threatinputStreamAds, "UTF-8"));
                    String threatlineAds = "";
                    while ((threatlineAds = threatbufferedReaderAds.readLine()) != null)
                        threatresultAds += threatlineAds;
                    System.out.println(threatresultAds);
                    threatinputStreamAds.close();
                } else
                    threatresultAds = "Did not work!";

            } catch (Exception e) {
                System.out.println("InputStream" + e.getLocalizedMessage());
            }

            try {
                String[] str = threatresultAds.split("<");
                System.out.println("Here" + str[0]);
                JSONArray threatJarrayAds = new JSONArray(str[0]);
                for (int i = 0; i < threatJarrayAds.length(); i++) {
                    JSONObject threatJasonobjectAds = null;
                    threatJasonobjectAds = threatJarrayAds.getJSONObject(i);
                    {
                        adsMap.put(threatJasonobjectAds.getString("App Name"),
                                new ArrayList<String>(Arrays.asList(threatJasonobjectAds.getString("Total"), threatJasonobjectAds.getString("MobileApp Tracking"))));
                    }
                }

                System.out.println("hello" + adsMap.size()
                        + adsMap.get("Funny Camera - Video Booth Fun"));

            } catch (Exception e1) {

                System.out.println("Error parsing data here2 " + e1.toString());

            }


            String threaturl = "http://androidpermission.comuv.com/ThreatsTable.php";
            InputStream threatinputStream = null;

            try {
                HttpClient threathttpclient = new DefaultHttpClient();

                HttpResponse threathttpResponse = threathttpclient.execute(new HttpGet(threaturl));
                threatinputStream = threathttpResponse.getEntity().getContent();

                if (threatinputStream != null) {
                    BufferedReader threatbufferedReader = new BufferedReader(
                            new InputStreamReader(threatinputStream, "UTF-8"));
                    String threatline = "";
                    while ((threatline = threatbufferedReader.readLine()) != null)
                        threatresult += threatline;
                    System.out.println(threatresult);
                    threatinputStream.close();
                } else
                    threatresult = "Did not work!";

            } catch (Exception e) {
                System.out.println("InputStream" + e.getLocalizedMessage());
            }
            //from here it starts

            try {
                JSONArray threatJarray = new JSONArray(threatresult);
                for (int i = 0; i < threatJarray.length(); i++) {
                    JSONObject threatJasonobject = null;
                    threatJasonobject = threatJarray.getJSONObject(i);
                    {
                        threatAndroidPermissionMultimap1.put(
                                threatJasonobject.getString("Android Application Permissions,Threats"),
                                new ArrayList<String>(Arrays.asList(threatJasonobject.getString("Android.Viser. A"),
                                        threatJasonobject.getString("Android Mobclick. A"),
                                        threatJasonobject.getString("Android.Agent.CE"),
                                        threatJasonobject.getString("Android.Domob.A"),
                                        threatJasonobject.getString("Android.Airpush.D"),
                                        threatJasonobject.getString("Android.Agent.DL"),
                                        threatJasonobject.getString("Android.AdMongo.A"),
                                        threatJasonobject.getString("Android.Infostealer.C"),
                                        threatJasonobject.getString("Android. Leadbolt.A"),
                                        threatJasonobject.getString("Android.FakeRun.A"),
                                        threatJasonobject.getString("Android.Wabek.A"),
                                        threatJasonobject.getString("Android.Oldboot"),
                                        threatJasonobject.getString("Android.MobileSpyware.SmsTracker"),
                                        threatJasonobject.getString("Android.Trojan.Sms.Send.B"),
                                        threatJasonobject.getString("Android.Bot.Notcompatible"),

                                        threatJasonobject.getString("Android.MobileSpyware.Ackposts"),
                                        threatJasonobject.getString("Android.Trojan.Wapsx"),
                                        threatJasonobject.getString("Android.MobileSpyware.SpyBubbl"),
                                        threatJasonobject.getString("Android.Backdoor.Advulna"),
                                        threatJasonobject.getString("Android.MobileSpyware.SpyMob.a"),
                                        threatJasonobject.getString("Android.Adware.Kuguo.A"),
                                        threatJasonobject.getString("Android.Backdoor.Ikangoo"),
                                        threatJasonobject.getString("Android.MobileSpyware.Spyoo"),
                                        threatJasonobject.getString("Android.Trojan.GGTracker"),
                                        threatJasonobject.getString("Android.Adware.ImadPush.A"),
                                        threatJasonobject.getString("Android.Downloader.Morepaks"),
                                        threatJasonobject.getString("Android.Airpush.G"),

                                        threatJasonobject.getString("Android.Gedma.A"),
                                        threatJasonobject.getString("Android.SMSreg.V"),
                                        threatJasonobject.getString("Android.Gemni.A"),
                                        threatJasonobject.getString("Android.SecApk.A"),
                                        threatJasonobject.getString("Android.VirusShield.A"),
                                        threatJasonobject.getString("Android.Agent.HD"),
                                        threatJasonobject.getString("Android.Trojan.Coogos.A!tr"),
                                        threatJasonobject.getString("Android.Adware.Uapush.A"),
                                        threatJasonobject.getString("Android.Dowgin.T"),
                                        threatJasonobject.getString("Android.Coogos.Ad388"),
                                        threatJasonobject.getString("Android.SmsThief.AI"),
                                        threatJasonobject.getString("Android.Fobus.A"),

                                        threatJasonobject.getString("Android.Airpush.C"),
                                        threatJasonobject.getString("Android.Smsreg.W"),
                                        threatJasonobject.getString("Android.Opfake.AG"),
                                        threatJasonobject.getString("Android.Simplocker.D"),
                                        threatJasonobject.getString("Android.Inis.A"),
                                        threatJasonobject.getString("Android.SeaWeth.D"),
                                        threatJasonobject.getString("Android.Dingwe.A"),
                                        threatJasonobject.getString("Android.Koler.A"),
                                        threatJasonobject.getString("Android.Simplocker.A"),
                                        threatJasonobject.getString("Android.Selfmite.B"),
                                        threatJasonobject.getString("Android.MobileSpyware.Kasandra"),
                                        threatJasonobject.getString("Android.MobileSpyware.Gappusin"),

                                        threatJasonobject.getString("Android.Trojan.FakeFlash"),
                                        threatJasonobject.getString("Android.MobileSpyware.CellSpy"),
                                        threatJasonobject.getString("Android.MobileSpyware.Tekwon.A"),
                                        threatJasonobject.getString("Android.Trojan.Qdplugin"),
                                        threatJasonobject.getString("Android.ScareWare.Slocker.A"),
                                        threatJasonobject.getString("Android.Agent.KB"),
                                        threatJasonobject.getString("Android.Adend.A"),
                                        threatJasonobject.getString("Android.Wroba.A"),
                                        threatJasonobject.getString("Android.RevMobAD.A"),
                                        threatJasonobject.getString("Android.SmsTheif.BG"),
                                        threatJasonobject.getString("Android.Inmobi.A"),
                                        threatJasonobject.getString("Android.Svpeng.K"),

                                        threatJasonobject.getString("Trojan-SMS.AndroidOS.Podec.a"),
                                        threatJasonobject.getString("DangerousObject.Multi.Generic"),
                                        threatJasonobject.getString("Trojan.AndroidOS.Rootnik.a"),
                                        threatJasonobject.getString("Trojan-SMS.AndroidOS.Opfake.a"),
                                        threatJasonobject.getString("Backdoor.AndroidOS.Obad.f"),
                                        threatJasonobject.getString("Trojan-Downloader.AndroidOS.Leech.a"),
                                        threatJasonobject.getString("Exploit.AndroidOS.Lotoor.be"),
                                        threatJasonobject.getString("Trojan.AndroidOS.Ztorg.a"),
                                        threatJasonobject.getString("Trojan-Dropper.AndroidOS.Gorpo.a"),
                                        threatJasonobject.getString("Trojan.AndroidOS.Fadeb.a"),
                                        threatJasonobject.getString("Trojan-SMS.AndroidOS.Stealer.a"),
                                        threatJasonobject.getString("Exploit.AndroidOS.Lotoor.a"),

                                        threatJasonobject.getString("Trojan-SMS.AndroidOS.Opfake.bo"),
                                        threatJasonobject.getString("Trojan.AndroidOS.Ztorg.b"),
                                        threatJasonobject.getString("Trojan.AndroidOS.Mobtes.b"),
                                        threatJasonobject.getString("Trojan-SMS.AndroidOS.FakeInst.fz"),
                                        threatJasonobject.getString("Trojan.AndroidOS.Ztorg.pac"),
                                        threatJasonobject.getString("Trojan-SMS.AndroidOS.FakeInst.hb"),
                                        threatJasonobject.getString("Android.Smsreg.DA"),
                                        threatJasonobject.getString("Android.Sprovider.A"),
                                        threatJasonobject.getString("Android.Ztorg.A"),
                                        threatJasonobject.getString("Android.Leech.E"),
                                        threatJasonobject.getString("Android.Rootnik.C"),
                                        threatJasonobject.getString("Android.Reaper.A"),

                                        threatJasonobject.getString("Android.CallPay.A"),
                                        threatJasonobject.getString("Android.Senrec.A"),
                                        threatJasonobject.getString("Android.Mkero.A"),
                                        threatJasonobject.getString("Android.Feabme.A")

                                )));
                    }
                }

                System.out.println("purchase/n" + threatAndroidPermissionMultimap1.get("In app purchase"));
                int[] threatarray = new int[100];
                for (int i = 0; i < 100; i++) {
                    threatarray[i] = 0;
                }
                    /*ArrayList<String> apppermissions = new ArrayList<String>();
					apppermissions.add("Device & app history");
					apppermissions.add("Identity");*/


                for (int k = 0; k < listOfPermissions.size(); k++) {
                    //System.out.println("PermissionList.size()" + listOfPermissions.size());
                    ArrayList<String> arraylist = new ArrayList<String>();
                    try {
                        arraylist.addAll(threatAndroidPermissionMultimap1.get(listOfPermissions.get(k)));
                    } catch (NullPointerException e) {
                        continue;
                    }
                    //System.out.println("Check"+listOfPermissions.get(k) + "\n");
                    for (int i = 0; i < arraylist.size(); i++) {
                        if (arraylist.get(i).equalsIgnoreCase("1")) {
                            threatarray[i] = 1;
                            //	System.out.println("Permission " + arraylist.get(i) + "\n");
                        }
                    }
                }
                //System.out.println("here");
                int j = 0;
                int sum = 0;
                for (int i = 0; i < 99; i++) {
                    sum = sum + threatarray[i];
                }
                //System.out.println("Sum=" + sum);
                totalthreats = sum;
                toalnoofthreats = Integer.toString(sum);

            } catch (Exception e1) {

                System.out.println("Error parsing data here1 " + e1.toString());
            }
            //SVM Implementation starts here----------------------

            String threaturl5 = "http://androidpermission.comuv.com/SVMFeatures.php";
            InputStream threatinputStream5 = null;

            try {
                HttpClient threathttpclient = new DefaultHttpClient();

                HttpResponse threathttpResponse = threathttpclient.execute(new HttpGet(threaturl5));
                threatinputStream5 = threathttpResponse.getEntity().getContent();


                if (threatinputStream5 != null) {
                    BufferedReader threatbufferedReader = new BufferedReader(
                            new InputStreamReader(threatinputStream5, "UTF-8"));
                    String threatline = "";
                    while ((threatline = threatbufferedReader.readLine()) != null)
                        threatresult5 += threatline;
                    System.out.println(threatresult5);
                    threatinputStream5.close();
                } else
                    threatresult5 = "Did not work!";

            } catch (Exception e) {
                System.out.println("InputStream here" + e.getLocalizedMessage());
            }

            try {
                JSONArray threatJarray = new JSONArray(threatresult5);
                System.out.println("FUCK"+threatresult5);
                for (int i = 0; i < threatJarray.length(); i++) {
                    JSONObject threatJasonobject = null;
                    threatJasonobject = threatJarray.getJSONObject(i);
                    {
                        threatAndroidPermissionMultimap3.put(
                                threatJasonobject.getString("autoincrement"),
                                new ArrayList<String>(Arrays.asList(threatJasonobject.getString("Feature1DC"),
                                        threatJasonobject.getString("Feature2Ads"),
                                        threatJasonobject.getString("Feature3threats"),
                                        threatJasonobject.getString("Feature4OP"),
                                        threatJasonobject.getString("Feature5Range"),
                                        threatJasonobject.getString("MBS"),
                                        threatJasonobject.getString("CodeNotation")

                                )));
                    }
                }

                System.out.println("SVM/n" + threatAndroidPermissionMultimap3.get("3"));
                ArrayList<String> arraylist1 = new ArrayList<String>();
                System.out.println("Features"+feature1+feature2+feature3+feature4+feature5);
                for(int i=0;i<threatAndroidPermissionMultimap3.size();i++)
                {
                    i=i+1;
                    arraylist1.addAll(threatAndroidPermissionMultimap3.get(Integer.toString(i)));
                    if(feature1.equalsIgnoreCase(arraylist1.get(0)) && (feature2.equalsIgnoreCase(arraylist1.get(1))) && (feature3.equalsIgnoreCase(arraylist1.get(2))) && (feature4.equalsIgnoreCase(arraylist1.get(3))) && (feature5.equalsIgnoreCase(arraylist1.get(4))))
                    {
                        System.out.println("Final output"+arraylist1.get(5));
                    }
                }


            } catch (Exception e1) {

                System.out.println("Error parsing data here1 " + e1.toString());
            }
            //SVM implementation ends here------
            return null;

        }

        protected void onPostExecute(Void v) {
            System.out.println("We are here2 \n");
            if (connectionURL.equalsIgnoreCase("Failed")) {
                //TxtSubcatName2.setText("App Not found in Google Play Store");
                TxtCatagoryName.setText("App Not found in Google Play Store");
                this.progressDialog.dismiss();
            } else {
                TextView Txt1 = (TextView) findViewById(R.id.textView1);
                //	System.out.println("Name="+)
                progressDialog.cancel();
                //Txt1.setText(html.toString());

			/*Elements newsHeadlines1 = html.select("div.info-container").select("div.info-container").
					select("a").select(".document-subtitle").select("span[itemprop=\"genre\"]");*/

                Elements SubcatEle = html.select("div.info-container").select("div.info-container").
                        select("a").select(".document-subtitle").select("span[itemprop=\"name\"]");
                Elements newsHeadlines = html.select("div.info-container").select("div.info-container").
                        select("a").select(".document-subtitle").select("span[itemprop=\"genre\"]");
                Element doc = html.select("div.details-info").select("div.info-container").
                        select("div.details-actions").select("span").select("button").select("span").last();
                // Elements elements = html.select("div.info-container").select("div.info-container > span.genre");
                Element newsHeadlines2 = html.select("div.info-container").select("div.info-container").
                        select("a").select(".document-subtitle").select("span[itemprop=\"genre\"]").last();
                Elements AppPurchase = html.select("div.info-container").select("div.info-container")
                        .select("div.inapp-msg");
                Elements AddSupport = html.select("div.info-container").select("div.info-container")
                        .select("span.ads-supported-label-msg");
                org.jsoup.nodes.Element elemet = newsHeadlines.first();
                AppCatagoryName = elemet.ownText();
                PaidorFree = doc.ownText();
                SubcatName = SubcatEle.text();
                String subcat2 = newsHeadlines2.text();
                CpyAppPurchase = AppPurchase.text();
                CpyAddSupport = AddSupport.text();
                this.progressDialog.dismiss();

                if (subcat2.equalsIgnoreCase("") || subcat2.equalsIgnoreCase(AppCatagoryName)) {
                    TxtSubcatName2.setText("");
                } else {
                    //Rahul ChangesTxtSubcatName2.setText(subcat2);
                    TxtSubcatName2.setText("");
                }

//Testing


				/*try {
					JSONArray threatJarray = new JSONArray(threatresult);
					for (int i = 0; i < threatJarray.length(); i++) {
						JSONObject threatJasonobject = null;
						threatJasonobject = threatJarray.getJSONObject(i);
						{
							threatAndroidPermissionMultimap1.put(
									threatJasonobject.getString("Android Application Permissions,Threats"),
									new ArrayList<String>(Arrays.asList(threatJasonobject.getString("Android.Viser. A"),
											threatJasonobject.getString("Android Mobclick. A"),
											threatJasonobject.getString("Android.Agent.CE"),
											threatJasonobject.getString("Android.Domob.A"),
											threatJasonobject.getString("Android.Airpush.D"),
											threatJasonobject.getString("Android.Agent.DL"),
											threatJasonobject.getString("Android.AdMongo.A"),
											threatJasonobject.getString("Android.Infostealer.C"),
											threatJasonobject.getString("Android. Leadbolt.A"),
											threatJasonobject.getString("Android.FakeRun.A"),
											threatJasonobject.getString("Android.Wabek.A"),
											threatJasonobject.getString("Android.Oldboot"),
											threatJasonobject.getString("Android.MobileSpyware.SmsTracker"),
											threatJasonobject.getString("Android.Trojan.Sms.Send.B"),
											threatJasonobject.getString("Android.Bot.Notcompatible"),

											threatJasonobject.getString("Android.MobileSpyware.Ackposts"),
											threatJasonobject.getString("Android.Trojan.Wapsx"),
											threatJasonobject.getString("Android.MobileSpyware.SpyBubbl"),
											threatJasonobject.getString("Android.Backdoor.Advulna"),
											threatJasonobject.getString("Android.MobileSpyware.SpyMob.a"),
											threatJasonobject.getString("Android.Adware.Kuguo.A"),
											threatJasonobject.getString("Android.Backdoor.Ikangoo"),
											threatJasonobject.getString("Android.MobileSpyware.Spyoo"),
											threatJasonobject.getString("Android.Trojan.GGTracker"),
											threatJasonobject.getString("Android.Adware.ImadPush.A"),
											threatJasonobject.getString("Android.Downloader.Morepaks"),
											threatJasonobject.getString("Android.Airpush.G"),

											threatJasonobject.getString("Android.Gedma.A"),
											threatJasonobject.getString("Android.SMSreg.V"),
											threatJasonobject.getString("Android.Gemni.A"),
											threatJasonobject.getString("Android.SecApk.A"),
											threatJasonobject.getString("Android.VirusShield.A"),
											threatJasonobject.getString("Android.Agent.HD"),
											threatJasonobject.getString("Android.Trojan.Coogos.A!tr"),
											threatJasonobject.getString("Android.Adware.Uapush.A"),
											threatJasonobject.getString("Android.Dowgin.T"),
											threatJasonobject.getString("Android.Coogos.Ad388"),
											threatJasonobject.getString("Android.SmsThief.AI"),
											threatJasonobject.getString("Android.Fobus.A"),

											threatJasonobject.getString("Android.Airpush.C"),
											threatJasonobject.getString("Android.Smsreg.W"),
											threatJasonobject.getString("Android.Opfake.AG"),
											threatJasonobject.getString("Android.Simplocker.D"),
											threatJasonobject.getString("Android.Inis.A"),
											threatJasonobject.getString("Android.SeaWeth.D"),
											threatJasonobject.getString("Android.Dingwe.A"),
											threatJasonobject.getString("Android.Koler.A"),
											threatJasonobject.getString("Android.Simplocker.A"),
											threatJasonobject.getString("Android.Selfmite.B"),
											threatJasonobject.getString("Android.MobileSpyware.Kasandra"),
											threatJasonobject.getString("Android.MobileSpyware.Gappusin"),

											threatJasonobject.getString("Android.Trojan.FakeFlash"),
											threatJasonobject.getString("Android.MobileSpyware.CellSpy"),
											threatJasonobject.getString("Android.MobileSpyware.Tekwon.A"),
											threatJasonobject.getString("Android.Trojan.Qdplugin"),
											threatJasonobject.getString("Android.ScareWare.Slocker.A"),
											threatJasonobject.getString("Android.Agent.KB"),
											threatJasonobject.getString("Android.Adend.A"),
											threatJasonobject.getString("Android.Wroba.A"),
											threatJasonobject.getString("Android.RevMobAD.A"),
											threatJasonobject.getString("Android.SmsTheif.BG"),
											threatJasonobject.getString("Android.Inmobi.A"),
											threatJasonobject.getString("Android.Svpeng.K"),

											threatJasonobject.getString("Trojan-SMS.AndroidOS.Podec.a"),
											threatJasonobject.getString("DangerousObject.Multi.Generic"),
											threatJasonobject.getString("Trojan.AndroidOS.Rootnik.a"),
											threatJasonobject.getString("Trojan-SMS.AndroidOS.Opfake.a"),
											threatJasonobject.getString("Backdoor.AndroidOS.Obad.f"),
											threatJasonobject.getString("Trojan-Downloader.AndroidOS.Leech.a"),
											threatJasonobject.getString("Exploit.AndroidOS.Lotoor.be"),
											threatJasonobject.getString("Trojan.AndroidOS.Ztorg.a"),
											threatJasonobject.getString("Trojan-Dropper.AndroidOS.Gorpo.a"),
											threatJasonobject.getString("Trojan.AndroidOS.Fadeb.a"),
											threatJasonobject.getString("Trojan-SMS.AndroidOS.Stealer.a"),
											threatJasonobject.getString("Exploit.AndroidOS.Lotoor.a"),

											threatJasonobject.getString("Trojan-SMS.AndroidOS.Opfake.bo"),
											threatJasonobject.getString("Trojan.AndroidOS.Ztorg.b"),
											threatJasonobject.getString("Trojan.AndroidOS.Mobtes.b"),
											threatJasonobject.getString("Trojan-SMS.AndroidOS.FakeInst.fz"),
											threatJasonobject.getString("Trojan.AndroidOS.Ztorg.pac"),
											threatJasonobject.getString("Trojan-SMS.AndroidOS.FakeInst.hb"),
											threatJasonobject.getString("Android.Smsreg.DA"),
											threatJasonobject.getString("Android.Sprovider.A"),
											threatJasonobject.getString("Android.Ztorg.A"),
											threatJasonobject.getString("Android.Leech.E"),
											threatJasonobject.getString("Android.Rootnik.C"),
											threatJasonobject.getString("Android.Reaper.A"),

											threatJasonobject.getString("Android.CallPay.A"),
											threatJasonobject.getString("Android.Senrec.A"),
											threatJasonobject.getString("Android.Mkero.A"),
											threatJasonobject.getString("Android.Feabme.A")

									)));
						}
					}

					//System.out.println("/n" + threatAndroidPermissionMultimap1.get("In app purchase"));
					int[] threatarray = new int[100];
					for (int i = 0; i < 100; i++) {
						threatarray[i] = 0;
					}
					/*ArrayList<String> apppermissions = new ArrayList<String>();
					apppermissions.add("Device & app history");
					apppermissions.add("Identity");*/


					/*for (int k = 0; k < listOfPermissions.size(); k++) {
						//System.out.println("PermissionList.size()" + listOfPermissions.size());
						ArrayList<String> arraylist = new ArrayList<String>();
						try {
							arraylist.addAll(threatAndroidPermissionMultimap1.get(listOfPermissions.get(k)));
						}
						catch(NullPointerException e)
						{
							continue;
						}
						//System.out.println("Check"+listOfPermissions.get(k) + "\n");
						for (int i = 0; i < arraylist.size(); i++) {
							if (arraylist.get(i).equalsIgnoreCase("1")) {
								threatarray[i] = 1;
								//	System.out.println("Permission " + arraylist.get(i) + "\n");
							}
						}
					}
					//System.out.println("here");
					int j = 0;
					int sum = 0;
					for (int i = 0; i < 99; i++) {
						sum = sum + threatarray[i];
					}
					//System.out.println("Sum=" + sum);
					toalnoofthreats=Integer.toString(sum);

				} catch (Exception e1) {

					System.out.println("Error parsing data " + e1.toString());
				}*/

                try {
                    //System.out.println("one");
                    JSONArray raJarray = new JSONArray(raresult);
                    for (int i = 0; i < raJarray.length(); i++) {
                        //System.out.println("two");
                        JSONObject raJasonobject = null;
                        raJasonobject = raJarray.getJSONObject(i);
                        {
                            //System.out.println("three");
                            raAndroidPermissionMultimap1.put(raJasonobject.getString("CatagoryName"),
                                    new ArrayList<String>(Arrays.asList(raJasonobject.getString("MinPermissions"),
                                            raJasonobject.getString("MaxPermissions"), raJasonobject.getString("AvgMinMax"),
                                            raJasonobject.getString("MinFreePermissions"),
                                            raJasonobject.getString("MaxFreePermissions"),
                                            raJasonobject.getString("AvgFreeMinMax"),
                                            raJasonobject.getString("MinPaidPermissions"),
                                            raJasonobject.getString("MaxPaidPermissions"),
                                            raJasonobject.getString("AvgPaidMinMax"))));
                        }
                    }
                    System.out.println("four");
                    //System.out.println("/n" + raAndroidPermissionMultimap1.get(AppCatagoryName.toString()));
                    ArrayList<String> arraylist = new ArrayList<String>();
                    System.out.println("five");
                    arraylist.addAll(raAndroidPermissionMultimap1.get(AppCatagoryName.toString()));
                    System.out.println("six");
                    //System.out.println("Family: Music & Video" + "\n");
                    //for (int i = 0; i < arraylist.size(); i++) {
                    //	System.out.println("Permission " + arraylist.get(i) + "\n");

                    //}
                    for (int i = 0; i < listOfPermissions.size(); i++) {
                        //Log.d("\n Got it got it got it ",PermissionList.get(i).toString());
                        //Log.d("\n Got it got it got it ",PermissionList.get(i).toString());
                        //System.out.println("Problem"+listOfPermissions.get(i));
                    }
                    System.out.println("Rahul6");
                    if (PaidorFree.equalsIgnoreCase("Install")) {
                        int minfreepermission = Integer.parseInt(arraylist.get(3));
                        int maxfreepermission = Integer.parseInt(arraylist.get(4));
                        if (PermissionList.size() >= maxfreepermission) {
                            if (CpyAppPurchase.equalsIgnoreCase("Offers in-app purchases")) {
                                feature4 = "1";
                                if (totalthreats > 70) {
                                    feature3 = "1";
                                } else {
                                    feature3 = "0";
                                }
                                TxtPaidOrFree.setText("Free" + " " + "OverLoad" + PermissionList.size() + "Threat" + toalnoofthreats);
                                feature5 = "1";
                                System.out.println("{0,0,0,0,1}");
                                System.out.println("Rahulif");
                                TxtCatagoryName.append("Free");
                                TxtPaidOrFree.setTextColor(Color.RED);
								/*for(int i=0;i<PermissionList.size();i++)
								{
									Log.d("\n Got it got it got it ",PermissionList.get(i).toString());
									Log.d("\n Got it got it got it ",PermissionList.get(i).toString());
								}*/
                            } else {
                                if (totalthreats > 70) {
                                    feature3 = "1";
                                } else {
                                    feature3 = "0";
                                }
                                TxtPaidOrFree.setText("Free" + " " + "OverLoad" + PermissionList.size() + " Threat" + toalnoofthreats);
                                System.out.println("{0,0,0,0,1}");
                                feature5 = "1";
                                TxtCatagoryName.append("Free");
                            }
                        } else {
                            feature5 = "0";

                            if (CpyAppPurchase.equalsIgnoreCase("Offers in-app purchases")) {
                                feature4 = "1";
                                if (totalthreats > 70) {
                                    feature3 = "1";
                                } else {
                                    feature3 = "0";
                                }
                                System.out.println("Rahulinapppurchase");
                                TxtPaidOrFree.setText("Free" + " " + " Threat" + toalnoofthreats);
                                TxtCatagoryName.append("Free");
                                TxtPaidOrFree.setTextColor(Color.RED);
                            } else {
                                if (totalthreats > 70) {
                                    feature3 = "1";
                                } else {
                                    feature3 = "0";
                                }
                                System.out.println("Rahul5");
                                TxtPaidOrFree.setText("Free" + " " + " Threat" + toalnoofthreats);
                                TxtCatagoryName.append("Free");
                            }


                        }
                    } else {
                        System.out.println("Rahul1");
                        int maxpaidpermission = Integer.parseInt(arraylist.get(7));
                        if (PermissionList.size() >= maxpaidpermission) {
                            if (totalthreats > 70) {
                                feature3 = "1";
                            } else {
                                feature3 = "0";
                            }
                            System.out.println("Rahul2");
                            TxtPaidOrFree.setText("Paid" + " " + "OverLoad" + PermissionList.size() + " Threat" + toalnoofthreats);
                            feature5 = "1";
                            TxtCatagoryName.append("Paid");

                        } else {
                            feature5 = "0";
                            if (totalthreats > 70) {
                                feature3 = "1";
                            } else {
                                feature3 = "0";
                            }
                            System.out.println("Rahul3");
                            TxtPaidOrFree.setText("Paid" + " " + " Threat" + toalnoofthreats);
                            TxtCatagoryName.append("Paid");
                        }

                    }
                } catch (Exception e1) {
                    // TODO: handle exception
                    Log.d("Error parsing data ", e1.toString());
                    System.out.println("Error parsing data here3 " + e1.toString());
                }


                TxtCatagoryName.setText(AppCatagoryName.toString());


                try {
                    String val = adsMap.get(SubcatName).get(0);
                    String val2 = adsMap.get(SubcatName).get(1);
                    int value2 = Integer.parseInt(val2);
                    int value1 = Integer.parseInt(val);
                    if (value1 >= 0) {
                        if (SubcatName.equalsIgnoreCase("")) {
                            TxtSubcatName.setText("");
                        } else {
                            if (SubcatName.equalsIgnoreCase("Facebook")) {
                                feature1 = "1";
                                if (value2 == 1) {
                                    TxtSubcatName.setText(SubcatName + " Total no of Ads=" + value1 + " includes MobileAppTracking" + " Dangerous Combination Exist");
                                    if (value1 >= 5) {
                                        feature2 = "1";
                                    }
                                } else
                                    TxtSubcatName.setText(SubcatName + " Total no of Ads=" + value1 + " Dangerous Combination Exist");
                                //String text2 = "<font color=#FF0000>Total no of Ads=</font>";
                                //if(CpyAddSupport.equalsIgnoreCase("Ad-supported family app"))
                                //TxtSubcatName.append(Html.fromHtml(text2));
                                feature2 = "0";
                                TxtSubcatName.setTextColor(Color.RED);
                            } else {
                                feature1 = "0";
                                if (value2 == 1) {
                                    TxtSubcatName.setText(SubcatName + " Total no of Ads=" + value1 + " includes MobileAppTracking");
                                    if (value1 >= 5) {
                                        feature2 = "1";
                                    }
                                } else
                                    TxtSubcatName.setText(SubcatName + " Total no of Ads=" + value1);
                                //String text2 = "<font color=#FF0000>Total no of Ads=</font>";
                                //if(CpyAddSupport.equalsIgnoreCase("Ad-supported family app"))
                                //TxtSubcatName.append(Html.fromHtml(text2));
                                feature2 = "0";
                                TxtSubcatName.setTextColor(Color.RED);
                            }
                        }
                    }
                } catch (Exception e) {
                    if (SubcatName.equalsIgnoreCase("")) {
                        TxtSubcatName.setText("");
                    } else {
                        TxtSubcatName.setText(SubcatName + "");
                        String text2 = "<font color=#FF0000>(Has Adds)</font>";
                        if (CpyAddSupport.equalsIgnoreCase("Ad-supported family app"))
                            TxtSubcatName.append(Html.fromHtml(text2));

                    }
                }
				/*if(SubcatName.equalsIgnoreCase("")){
					TxtSubcatName.setText("");
				}else{
					TxtSubcatName.setText(SubcatName+"Hello");
					String text2 = "<font color=#FF0000>(Has Adds)</font>";
					if(CpyAddSupport.equalsIgnoreCase("Ad-supported family app"))
						TxtSubcatName.append(Html.fromHtml(text2));
				}*/


				/*if(PaidorFree.equalsIgnoreCase("Install")){
					if(CpyAppPurchase.equalsIgnoreCase("Offers in-app purchases")){
					TxtPaidOrFree.setText("Free"+ " "+PermissionList.size());
					TxtPaidOrFree.setTextColor(Color.RED);
					}else{
						TxtPaidOrFree.setText("Free"+ " "+PermissionList.size());
					}
				}else
					TxtPaidOrFree.setText("Paid"+" "+ PermissionList.size());*/
            }
            System.out.println("Features here"+feature1+feature2+feature3+feature4+feature5);

            System.out.println("SVM 3/n" + threatAndroidPermissionMultimap3.get("3"));

            System.out.println("Features 3 "+feature1+feature2+feature3+feature4+feature5);
            System.out.println("Map size="+threatAndroidPermissionMultimap3.size());
            for(int i=1;i<=threatAndroidPermissionMultimap3.size();i++)
            {
                ArrayList<String> arraylist2 = new ArrayList<String>();
                arraylist2.addAll(threatAndroidPermissionMultimap3.get(Integer.toString(i)));
                System.out.println("Map value= " + threatAndroidPermissionMultimap3.get(Integer.toString(i)));
                        System.out.println("Arraylist" + arraylist2.get(0));
                if(feature1.equalsIgnoreCase(arraylist2.get(0)) && (feature2.equalsIgnoreCase(arraylist2.get(1))) && (feature3.equalsIgnoreCase(arraylist2.get(2))) && (feature4.equalsIgnoreCase(arraylist2.get(3))) && (feature5.equalsIgnoreCase(arraylist2.get(4))))
                {
                    System.out.println("Final output 3" + arraylist2.get(5));
                    if(arraylist2.get(5).equalsIgnoreCase("S"))
                    TxtPaidOrFree.append(" SAFE");
                    else if(arraylist2.get(5).equalsIgnoreCase("B"))
                        TxtPaidOrFree.append(" BENIGN");
                    else if(arraylist2.get(5).equalsIgnoreCase("M"))
                        TxtPaidOrFree.append(" MALICIOUS");

                }
            }

        }


    }


    class Listadapter extends BaseAdapter {
        Context context;
        ArrayList<String> myKey;
        LinkedHashMap<String, ArrayList<String>> sortedMap;

        //ArrayList PermissionList;
        public Listadapter(Context context, LinkedHashMap sortedMap, ArrayList<String> myKey) {
            this.context = context;
            this.sortedMap = sortedMap;
            this.myKey = myKey;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return sortedMap.size();
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            LayoutInflater inflator = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            View row = inflator.inflate(R.layout.list_permission_mapping, arg2, false);
            TextView CatagoryTitle = (TextView) row.findViewById(R.id.CatagoryTitle);
            TextView CatagoryName = (TextView) row.findViewById(R.id.CatagoryName);
            TextView CatagoryDetails = (TextView) row.findViewById(R.id.CatagoryDetail);
            TextView PermissionName = (TextView) row.findViewById(R.id.PermissionName);
            View viewname = (View) row.findViewById(R.id.view1);
            String DangerousLevel = sortedMap.get(myKey.get(arg0)).get(3);
            if (arg0 != 0) {
                String CopyCatagoryName = sortedMap.get(myKey.get(arg0 - 1)).get(0);
                if (CopyCatagoryName.equals(sortedMap.get(myKey.get(arg0)).get(0))) {
                    CatagoryTitle.setText("");
                    ((ViewGroup) viewname.getParent()).removeView(viewname);
                } else {
                    CatagoryTitle.setText(sortedMap.get(myKey.get(arg0)).get(0));
                }
            } else {
                CatagoryTitle.setText(sortedMap.get(myKey.get(arg0)).get(0));
            }
            if (DangerousLevel.equalsIgnoreCase("1")) {
                feature4 = "1";
                CatagoryName.setTextColor(Color.RED);
                PermissionName.setTextColor(Color.RED);
                CatagoryDetails.setTextColor(Color.RED);
            }
            CatagoryName.setText(myKey.get(arg0));


            ArrayList<String> arraylist1 = new ArrayList<String>();
            //System.out.println("111111111"+threatAndroidPermissionMultimap2.get("In app purchase"));
            //for(int i=0;i<threatAndroidPermissionMultimap2.size();i++)
            //{

            //System.out.println("2222222222222"+threatAndroidPermissionMultimap1.get("Device & app history"));
            //}
            try {
                //threatAndroidPermissionMultimap2.get("In app purchase").trimToSize();
                System.out.println("Inside try");
                System.out.println("threatAndroidPermissionMultimap2.get(\"In app purchase\")" + sortedMap.get(myKey.get(arg0)).get(1) + threatAndroidPermissionMultimap2.get("Purchase item").get(5));
                System.out.println("Count" + threatAndroidPermissionMultimap2.get("Purchase item").size());
                for (int i = 0; i < threatAndroidPermissionMultimap2.get(sortedMap.get(myKey.get(arg0)).get(1)).size(); i++) {
                    //System.out.println("threatAndroidPermissionMultimap2.get(\"In app purchase\")"+threatAndroidPermissionMultimap2.get("Identity").get(i));
                    try {
                        //if (threatAndroidPermissionMultimap2.get("In app purchase").get(i) == null) {

                        //} else {
                        //System.out.println("threatAndroidPermissionMultimap2.get(\"In app purchase\")" + threatAndroidPermissionMultimap2.get(sortedMap.get(myKey.get(arg0)).get(1)).get(i));
                        //arraylist1.add(threatAndroidPermissionMultimap2.get(sortedMap.get(myKey.get(arg0)).get(1)).get(i));
                        arraylist1.add(threatAndroidPermissionMultimap2.get(sortedMap.get(myKey.get(arg0)).get(1)).get(i));
                        //}
                    } catch (NullPointerException e) {
                        System.out.println("Error at line 5");
                        continue;
                    }
                }
            } catch (NullPointerException e) {

            }


            //System.out.println("2222222222222" + threatAndroidPermissionMultimap2.get("Identity"));
            totalThreats = 0;
            System.out.println("ArrayList Size" + arraylist1.size());
            for (int i = 0; i < arraylist1.size(); i++) {
                System.out.println("Inside array list" + arraylist1.get(i));
            }
            //	arraylist1.addAll(threatAndroidPermissionMultimap1.get(sortedMap.get(myKey.get(arg0)).get(1)));

            for (int i = 0; i < arraylist1.size(); i++) {
                try {
                    //if (arraylist1.get(i).equalsIgnoreCase("1")) {
                    int value = Integer.parseInt(arraylist1.get(i));
                    System.out.println("Value=" + value);
                    Log.d("seek to it ", arraylist1.get(i));
                    totalThreats = totalThreats + value;
                    //}
                } catch (Exception e) {
                    System.out.println("Error in line 1");
                    continue;
                }

            }
            System.out.println("totalThreats=" + totalThreats);
            //	}
			/*catch(NullPointerException e)
			{
				System.out.println("Problem"+e);
			}*/
            PermissionName.setText(sortedMap.get(myKey.get(arg0)).get(1) + " Threats =" + totalThreats);
            CatagoryDetails.setText(sortedMap.get(myKey.get(arg0)).get(2));
           /* System.out.println("Feature final= " + feature1 + feature2 + feature3 + feature4 + feature5);

           System.out.println("SVM/n" + threatAndroidPermissionMultimap3.get("3"));
            ArrayList<String> arraylist2 = new ArrayList<String>();
            System.out.println("Features"+feature1+feature2+feature3+feature4+feature5);
            for(int i=0;i<threatAndroidPermissionMultimap3.size();i++)
            {
                i=i+1;
                arraylist2.addAll(threatAndroidPermissionMultimap3.get(Integer.toString(i)));
                if(feature1.equalsIgnoreCase(arraylist2.get(0)) && (feature2.equalsIgnoreCase(arraylist2.get(1))) && (feature3.equalsIgnoreCase(arraylist2.get(2))) && (feature4.equalsIgnoreCase(arraylist2.get(3))) && (feature5.equalsIgnoreCase(arraylist2.get(4))))
                {
                    System.out.println("Final output"+arraylist2.get(5));
                }
            }*/
            return row;
        }

    }


}


